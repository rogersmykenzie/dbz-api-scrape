const puppeteer = require('puppeteer');
const massive = require('massive');
// const firebase = require('firebase');
const connectionString = ''; //censored
let database;
massive(connectionString).then(db => {
    database = db;
    console.log('Database Connected');
});

function getDatabase() {
    return database;
}
(async () => {
    let browser = await puppeteer.launch({headless: false}); //Launch the slave browser
    const charactersURL = "https://dragonball.fandom.com/wiki/Category:Characters"; //Starting URL
    let page = await browser.newPage(); //Open a new tab
    await page.goto(charactersURL, {waitUntil: 'networkidle2'}); //Goto the baseurl, wait until there are a maximum of 2 network connections
    let allChars = [];
    let bool = true;
    while(bool) { //For each page of characters
        let charURLs = await page.evaluate(async () => { //Get all the links the reference a certain characters...
            let liTags = document.getElementsByTagName('li'); //...page on the wiki
            let urls = [];
            for(let key in liTags) { //Add them all to an array
                if(liTags[key].className==="category-page__member") {
                    if(liTags[key].childNodes[3]) {
                        urls.push(liTags[key].childNodes[3].href);
                    }
                }
            }
            return urls; //Save all the characterURLs to an array
        })
        allChars.push(charURLs); //Add all the URLs from this page to an overall array.
        let newURL = await page.evaluate(async () => { //Here we try to get the href on the 'Next' button
            let obj = document.getElementsByTagName('a'); //We get all the a tags
            let href = '';
            for(let key in obj) { //If there is an a tag with the matching class name...
                if(obj[key].className === 'category-page__pagination-next wds-button wds-is-secondary') { 
                    href = obj[key].href; //...we get the href and return it.
                }
            }
            return href;
            // }
        })
        if(newURL) { //If we were able to find an href on the 'Next' button
            await page.goto(newURL, {waitUntil: 'networkidle2'}) //We goto it and then start the loop over again
        } else { //Otherwise...
            bool = false; //...We end the loop
        }
    }
    allChars = allChars.flat(); //We flatten out our allChars array, as currently it is composed of smaller arrays
    bool = true; //Here we reuse the variable bool because reduce, reuse, recycle
    let index = 0; //This is used to keep track of where we are in the allChars array;
    await page.goto(allChars[627], {waitUntil: 'networkidle2'}) //We go to the first element in the array
    console.log(allChars.flat().length);
    let infoArr = [];

    for(let i = 628; i < allChars.length; i++) {
        let response = await page.evaluate(async () => { //Here we get the following data for each character.
            let name, nickNames, temp1, imageURL, mangaDebut, japaneseName, animeDebut, movieDebut, 
            races, gender, heightCM, heightDumbAmerican, weightKG, weightDumbAmerican, dateOfBirth, datesOfDeath,
            occupation, allegiance, address, relatives, counterparts, mentors = null;
            if(document.querySelector('h2[data-source="RefName"]'))
                name = await document.querySelector('h2[data-source="RefName"]').innerText.replace(/\[.{0,}\]/g, ''); //Character Name
            if(document.querySelector('div[class="pi-data-value pi-font"]'))
                nickNames = await document.querySelector('div[class="pi-data-value pi-font"]').innerText.split('\n'); //Character NickName - need to remove footnotes
            if(document.querySelector('div[class="db-series"]'))
                temp1 = await document.querySelector('div[class="db-series"]').childNodes;
            let appearsIn = []; //Shows the Character Appears In
            if(temp1) {
                for(let i = 600; i < temp1.length; i++) {
                    appearsIn.push(temp1[i].childNodes[0].title);
                }
            }
            if(document.querySelector('img[class="pi-image-thumbnail"]'))
                imageURL = await document.querySelector('img[class="pi-image-thumbnail"]').src;
            if(document.querySelector('div[data-source="manga debut"] > div > a')) 
                mangaDebut = await document.querySelector('div[data-source="manga debut"] > div > a').innerText;
            if(document.querySelector('h2[data-source="JapName"]'))
                japaneseName = await document.querySelector('h2[data-source="JapName"]').innerText;
            if(document.querySelector('div[data-source="anime debut"] > div > a')) 
                animeDebut = await document.querySelector('div[data-source="anime debut"] > div > a').innerText;
            if(document.querySelector('div[data-source="movie debut"] > div > i > a'))
                movieDebut = await document.querySelector('div[data-source="movie debut"] > div > i > a').innerText;
            if(document.querySelector('div[data-source="Race"] > div')) {
                let tempArr = await document.querySelector('div[data-source="Race"] > div').childNodes;
                if(tempArr) {
                    let arr = [];
                    for(let i = 0; i < tempArr.length; i++) {
                        let name = tempArr[i].className;
                        if(name !== 'reference' && tempArr[i].innerText) {
                            arr.push(tempArr[i].innerText);
                        }
                    }
                    for(let i = arr.length-1; i > 0; i--) {
                        if(arr[i].includes('('));
                        arr[i-1] = `${arr[i-1]} ${arr[i]}`;
                        arr.splice(i, 1);
                        i-=1;
                    }
                    races = arr.slice();
                }
            }
            if(document.querySelector('div[data-source="Gender"] > div')) {
                gender = await document.querySelector('div[data-source="Gender"] > div').innerText
            }
            if(document.querySelector('div[data-source="Height"] > div')) {
                let elem = await document.querySelector('div[data-source="Height"] > div').innerText.replace(/\[.{0,}\]/g, '')
                heightCM = elem.substring(0, elem.indexOf('cm') + 2);
                heightDumbAmerican = elem.substring(elem.indexOf('cm') + 3, elem.length);
            }
            if(document.querySelector('div[data-source="Weight"] > div')) {
                let elem = await document.querySelector('div[data-source="Weight"] > div').innerText.replace(/\[.{0,}\]/g, '');
                weightKG = elem.substring(0, elem.indexOf('kg') + 2);
                weightDumbAmerican = elem.substring(elem.indexOf('kg') + 3, elem.length);
            }
            if(document.querySelector('div[data-source="Date of birth"] > div > a')) {
                dateOfBirth = await document.querySelector('div[data-source="Date of birth"] > div > a').innerText;
            }
            if(document.querySelector('div[data-source="Date of death"] > div[class="pi-data-value pi-font"]')) {
                datesOfDeath = await document.querySelector('div[data-source="Date of death"] > div[class="pi-data-value pi-font"]').innerText.replace(/\[.{0,}\]/g, '').split('\n')
            }
            if(document.querySelector('div[data-source="Occupation"] > div')) {
                occupation = await document.querySelector('div[data-source="Occupation"] > div').innerText;
            }
            if(document.querySelector('div[data-source="Allegiance"] > div')) {
                allegiance = await document.querySelector('div[data-source="Allegiance"] > div').innerText.split('\n');
            }
            if(document.querySelector('div[data-source="Address"] > div')) {
                address = await document.querySelector('div[data-source="Address"] > div').innerText.replace(/\[.{0,}\]/g, '')
            }
            if(document.querySelector('div[data-source="FamConnect"] > div')) { //Here it would be cool to lookup an id based on the href of the a tag and use the index pos as an id
                relatives = await document.querySelector('div[data-source="FamConnect"] > div').innerText.replace(/\[.{0,}\]/g, '').split('\n')
            }
            if(document.querySelector('div[data-source="Counterparts"] > div > a')) {
                counterparts = await document.querySelector('div[data-source="Counterparts"] > div > a').innerText
            }
            if(document.querySelector('div[data-source="Mentors"] > div')) {
                mentors = await document.querySelector('div[data-source="Mentors"] > div').innerText.split('\n').map(val => val.replace(/\[.{0,}\]/g, ''))
            }
            return [
                name,
                japaneseName,
                nickNames,
                imageURL,
                appearsIn,
                mangaDebut,
                animeDebut,
                races,
                gender,
                heightCM,
                heightDumbAmerican,
                weightKG,
                weightDumbAmerican,
                dateOfBirth,
                datesOfDeath,
                occupation,
                allegiance,
                relatives,
                counterparts,
                mentors
            ]
            });

        await page.goto(allChars[i], {waitUntil: 'networkidle2'});
        let databaseData = response.map(val => {
            if(Array.isArray(val)) {
                if(val.length > 1) {
                    return val.join(',$1021');
                } else {
                    return val[0];
                }
            } else {
                return val;
            }
        })
        await database.insert_character(
            databaseData[0],
            databaseData[1],
            databaseData[2],
            databaseData[3],
            databaseData[4],
            databaseData[5],
            databaseData[6],
            databaseData[7],
            databaseData[8],
            databaseData[9],
            databaseData[10],
            databaseData[11],
            databaseData[12],
            databaseData[13],
            databaseData[14],
            databaseData[15],
            databaseData[16],
            databaseData[17],
            databaseData[18],
            databaseData[19],
        )
        console.log(i);
    }
    await browser.close();
})()